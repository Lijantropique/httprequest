# Bonjour, encoding: utf-8!
"""
==========================
Lijantropique Inc.
Title: Basic HTTP requests with python
Example argparse
lijantropique@protonmail.com
==========================
"""
import argparse
from bs4 import BeautifulSoup as bs4
import re
import requests

def arg():
    """
    Read basic arguments from the command line
    """
    ap = argparse.ArgumentParser()
    ap.add_argument('url', metavar='url', type=str, help='target url (e.g., http://10.10.10.10/mypage.php)')
    ap.add_argument('method', metavar='method', type=str, help='http method to be used with the request (e.g., \'GET\', \'POST\')')
    # ... additional arguments that can be verified/modified
    args = vars(ap.parse_args())
    if validate(args):
        return args
    else:
        print("Arguments provided not valid")

def validate(args):
    """
    Verify the url structure is well defined, the HTTP method is valid and all the other arguments (headers, cookies, etc)
    are valid for further processing. NOTE: valid ≠ correct
    """
    # only URL and method are mandatory
    urlFlag, methodFlag =  False, False

    if re.match('https?://[._+&/a-zA-Z0-9]+', args['url']):
        urlFlag = True
    if args['method'] in ('GET', 'POST', 'PUT', 'HEAD', 'DELETE', 'PATCH', 'OPTIONS'):
        methodFlag = True
    #... validate other options

    return all((urlFlag, methodFlag))


class Request(object):
    """ Basic python request connection """

    def __init__(self, args):
        """
        Creates a Request object
        args: python dictionary with request details. 
        See request documentation (https://requests.readthedocs.io/en/master/) for additional information about valid values for each parameter.
        """
        self.initialize(args)
        self.session = requests.Session()
        self.connect()
    
    def initialize(self, args):
        """
        Checks if the parameter provided was included in the args dictionary. Otherwise, set a default value.
        Note: It makes no validation at all!
        """
        self.url =      args['url']
        self.method =   args['method']
        self.data =     args.get('data', {}) 
        self.params =   args.get('param', {})
        self.allow_redirects=  args.get('redirect', True) 
        self.headers =  args.get('headers', {})
        self.cookies =  args.get('cookies', None)
        self.proxies =  args.get('proxies', {})
    
    def connect(self):
        """ Send the HTTPS request to the server """
        _METHODS = {
            'GET':  lambda: self.session.get(self.url, params=self.params, headers=self.headers, cookies=self.cookies, allow_redirects=self.allow_redirects, proxies=self.proxies),
            'POST': lambda: self.session.post(self.url, params=self.params, data=self.data, headers=self.headers, allow_redirects=self.allow_redirects, proxies=self.proxies), 
            'HEAD':  lambda: self.session.head(self.url, params=self.params, headers=self.headers, cookies=self.cookies, allow_redirects=self.allow_redirects, proxies=self.proxies)
            # .. additional HTTP verbs
        }
        self.response = _METHODS[self.method]()
        self.headers = self.response.headers
        self.makeSoup()

    def makeSoup(self):
        """ Create Beautifulsoup object for further processing """
        raw = self.response.content.decode("utf-8")
        self.soup = bs4(raw, 'html.parser')

# ----------------- main program -----------------
if __name__ == '__main__':
    # python3 exampleArgparse.py http://127.0.0.1/login.php GET
    args = arg()
    print(args)
    request = Request(args)
    # args = {'url': 'http://127.0.0.1/login.php', 'method': 'GET'}
    token =  request.soup("input", {"name": "user_token"})[0]["value"]
    print(f'token: {token}')