# Bonjour, encoding: utf-8!
"""
==========================
Lijantropique Inc.
Title: Basic HTTP requests with python
lijantropique@protonmail.com

This script shows how to make basic HTTP requests (GET, POST, HEAD) using python3. It is possible to get the arguments from the CLI (i.e., cURL style)
or from the script. It also allows for certain basic manipulation (e.g., modify headers) but can be expanded based on a specific request. Finally, 
it shows how to get/print info from the server's response using the BeautifulSoup module.
==========================
"""

import argparse
from bs4 import BeautifulSoup as bs4
import re
import requests

# ----------------- request object -----------------
class Request(object):
    """ Basic python request connection """

    def __init__(self, args):
        """
        Creates a Request object
        args: python dictionary with request details. See request documentation (https://requests.readthedocs.io/en/master/)
        for additional information about valid values for each parameter.
        """
        self.initialize(args)
        self.session = requests.Session()
        self.connect()
    
    def initialize(self, args):
        """
        Checks if the parameter provided was included in the args dictionary. Otherwise, set a default value.
        Note: It makes no validation at all!
        """
        self.url =      args['url']
        self.method =   args['method']
        self.data =     args.get('data', {}) 
        self.params =   args.get('param', {})
        self.allow_redirects=  args.get('redirect', True) 
        self.files =    args.get('files', {})
        self.headers =  args.get('headers', {})
        self.cookies =  args.get('cookies', None)
        self.auth =     args.get('auth', None)
        self.timeout =  args.get('timeout', None)
        self.proxies =  args.get('proxies', {})
    
    def update(self, args):
        self.initialize(args)
        self.connect()

    def connect(self):
        """ Send the HTTPS request to the server """
        _METHODS = {
            'GET':  lambda: self.session.get(self.url, params=self.params, headers=self.headers, cookies=self.cookies, auth=self.auth, timeout=self.timeout, allow_redirects=self.allow_redirects, proxies=self.proxies),
            'POST': lambda: self.session.post(self.url, params=self.params, data=self.data, headers=self.headers, auth=self.auth, timeout=self.timeout, allow_redirects=self.allow_redirects, proxies=self.proxies), 
            'HEAD':  lambda: self.session.head(self.url, params=self.params, headers=self.headers, cookies=self.cookies, auth=self.auth, timeout=self.timeout, allow_redirects=self.allow_redirects, proxies=self.proxies)
            # .. additional HTTP verbs
        }
        self.response = _METHODS[self.method]()
        self.headers = self.response.headers
        self.makeSoup()

    def makeSoup(self):
        """ Create Beautifulsoup object for further processing """
        raw = self.response.content.decode("utf-8")
        self.soup = bs4(raw, 'html.parser')


# ----------------- main program -----------------
if __name__ == '__main__':
    # ************ GET request example ************
    print ('1 - GET request example ')
    args = {'url': 'http://127.0.0.1/login.php', 'method': 'GET'}
    request = Request(args)
    content = request.soup("div", {"id":"content"})
    print(content)
    print()

    # ************ POST request example ************
    print ('2 - POST request example ')
    data = {'username': 'user1', ' password': 'pass1', ' user_token': '1234', ' Login': 'Login'}
    args = {'url': 'http://127.0.0.1/login.php', 'method': 'POST', 'data': data }
    print('-------- print <form> ---------')
    request = Request(args)
    forms =  request.soup("form")
    print(f'forms: {forms}')
    print('-------- print <token> ---------')
    token =  request.soup("input", {"name": "user_token"})[0]["value"]
    print(f'token: {token}')
    print()
